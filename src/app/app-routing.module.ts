import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [{
  path: 'admin',
    loadChildren: ()=> import('./admin-managnetuser/admin-managnetuser.module').then(m=> m.AdminManagnetuserModule),
},
{
  path: 'profile',
    loadChildren: ()=> import('./my-profile/my-profile.module').then(m=> m.MyProfileModule),
},
{
  path: 'admin',
    loadChildren: ()=> import('./admin-manegementtag/admin-manegementtag.module').then(m=> m.AdminManegementtagModule),
},
{
  path: 'post',
    loadChildren: ()=> import('./post/post.module').then(m=> m.PostModule),
},
{
  path: 'post',
    loadChildren: ()=> import('./post-view/post-view.module').then(m=> m.PostViewModule),
},
{
  path: 'admin',
    loadChildren: ()=> import('./admin-managnetpost/admin-managnetpost.module').then(m=> m.AdminManagnetpostModule),
},
  {
    path: '',
    loadChildren: ()=> import('./home/home.module').then(m=> m.HomeModule),

  },
  {
  path: "auth",
  loadChildren: ()=> import('./auth/auth.module').then(m=> m.AuthModule)
  
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
