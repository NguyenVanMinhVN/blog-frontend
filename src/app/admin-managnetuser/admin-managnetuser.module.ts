import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AdminManagnetuserComponent } from './admin-managnetuser.component';
import { AdminManagementUsersRoutingModule } from './admin-usersrouting.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    AdminManagnetuserComponent
  ],
  imports: [
    CommonModule,
    AdminManagementUsersRoutingModule,
    ReactiveFormsModule, 
    FormsModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],

})
export class AdminManagnetuserModule { }
