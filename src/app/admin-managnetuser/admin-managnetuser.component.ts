import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { IUser } from '../interfaces/users.interface';
import { UsersadminManagement } from '../services/admin.service';
import { FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-admin-managnetuser',
  templateUrl: './admin-managnetuser.component.html',
  styleUrls: ['./admin-managnetuser.component.scss']
})

export class AdminManagnetuserComponent implements OnInit {

  @ViewChild('udatetemplate')
  detailModal!: TemplateRef<any>;

  formupdate = this.fb.group({
    id: null,
    username: null,
    password: null,
    fullname: null,
    phone: null,
    email: null,
  });

  page = 1;
  limit = 10;
  totalItem = 0;
  modalRef?: BsModalRef;

  accounts: IUser[]= [];
  constructor(private fb: FormBuilder, private usersadminManagement: UsersadminManagement, private modalService: BsModalService) {}
  ngOnInit(): void {
    this.getDataInit()
  }

  getDataInit(){
    this.usersadminManagement.getAccount(this.page, this.limit).subscribe((res) => {
      console.log("Res : ", res);
      if (res.docs){
        console.log("res.doc ", res.docs)
        this.accounts = res.docs;
        this.totalItem = res.totalDocs as any;
      } 
    });
  }

  onUpdate(item?: IUser){
    if (item?.id) {
      this.formupdate.patchValue(item as any);
    }
    this.modalRef = this.modalService.show(this.detailModal, {
      class: 'modal-lg modal-digalog-centered',
    });
  }
  onDelete(item?: IUser){
    if (item?.id) {
      this.usersadminManagement.deleteAccount(item?.id).subscribe((result) => {
        if (result) {
          this.getDataInit()
        }
      })
    }
  }

  pageChanged(event: PageChangedEvent): void {
    this.page = event.page;
    this.getDataInit();
  }

  onSubmit(){
    const dataSend = { ...this.formupdate.value as any }
    if (dataSend.id) {
      this.usersadminManagement.updateAccount(dataSend.id, dataSend).subscribe((result) => {
        if (result) {
          // this.getDataInit();
          // this.toastrService.success(
          //   'Update news successfully',
          //   'Successfully'
          // );
          this.getDataInit()
          this.modalRef?.hide();
        }
      })
    }
  }
}
