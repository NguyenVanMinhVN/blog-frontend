import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManagnetuserComponent } from './admin-managnetuser.component';

describe('AdminManagnetuserComponent', () => {
  let component: AdminManagnetuserComponent;
  let fixture: ComponentFixture<AdminManagnetuserComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdminManagnetuserComponent]
    });
    fixture = TestBed.createComponent(AdminManagnetuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
