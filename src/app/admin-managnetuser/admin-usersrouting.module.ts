import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminManagnetuserComponent } from './admin-managnetuser.component';
import { authGuard } from '../auth/guard/auth.guard';



const routes: Routes = [{
  path: 'users',
  component: AdminManagnetuserComponent,
  canActivate:[authGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementUsersRoutingModule { }
