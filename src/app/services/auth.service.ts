import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ILoginResponse, IUser, IUserLogin, IUserRegister } from "../interfaces/users.interface";
import { CookieService } from "ngx-cookie-service";
import { Router } from "@angular/router";
import { BehaviorSubject, distinctUntilChanged } from "rxjs";
import { ToastrService } from "ngx-toastr";

@Injectable({
    providedIn:'root'
})
export class AuthService{
    constructor(private httpClient: HttpClient, private cookieService: CookieService, private router: Router, private toastrService: ToastrService){}

    private authInfoSub = new BehaviorSubject<IUser|null> (null);
    public authInfo = this.authInfoSub
    .asObservable()
    .pipe(distinctUntilChanged());

    login(body: IUserLogin){
        return this.httpClient.post<ILoginResponse>("http://localhost:3000/api/users/Login", body).subscribe(result =>{
            if(result){
                this.saveTokenToCookie(result.token);
                this.router.navigateByUrl('/');
                this.getUser();
            }
        });
    }
    register(body: IUserRegister){
        return this.httpClient.post<IUser>("http://localhost:3000/api/users/regiter", body).subscribe((result) =>{
            if(result){
                this.router.navigateByUrl('/auth/login');
                this.toastrService.success(
                    "Tao tai khoan thanh cong"
                  );

            }
        },
        (error) => {
            // Xử lý lỗi từ API
            this.toastrService.error(error.error.message[0]);
            console.error("Er",error);
          }
        );
       
    }

    getUserId() {
        // Assume your token is stored in a variable called 'token'
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.getTokenFromCookie()}`
        });
    
        return this.httpClient.get<IUser>("http://localhost:3000/api/users/me", { headers })
    }

    getUser() {
        // Assume your token is stored in a variable called 'token'
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.getTokenFromCookie()}`
        });
    
        this.httpClient.get<IUser>("http://localhost:3000/api/users/me", { headers }).subscribe(result => {
            if (result) {
                console.log("result : ", result)
                this.authInfoSub.next(result);
            }
        });
    }

    logOut(){
        this.authInfoSub.next(null);
        this.router.navigateByUrl('/');
        this.deleteTokenFromCookie();
    }

    checkLogin(){
        if(this.getTokenFromCookie()){
            return true;
        }
        return false;
    }

    saveTokenToCookie(token?: string): void {
        if(token)
        this.cookieService.set('accessToken', token, 1); 
      }
    
      // Hàm để lấy token từ cookie
      getTokenFromCookie(): string | undefined {
        return this.cookieService.get('accessToken');
      }
    
      // Hàm để xóa token khỏi cookie
      deleteTokenFromCookie(): void {
        const res = this.cookieService.delete('accessToken');
        console.log("this.cookieService.get('accessToken') ", res)
      }
}