import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { IPosts } from "../interfaces/post.interface";
import { IResponse } from "../interfaces/response.interface";
import { IUser } from "../interfaces/users.interface";

@Injectable({
    providedIn:'root'
})

export class PostService{
    constructor(private httpClient: HttpClient, private router: Router, private authService: AuthService){

    }

    creatPost(body: IPosts){
        console.log("Token : ", this.authService.getTokenFromCookie())
        
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.post<IPosts>("http://localhost:3000/api/posts", body,{ headers })
    }

    getPostId(id: string){
        return this.httpClient.get<IPosts>(`http://localhost:3000/api/posts/${id}`)
    }

    get_Authorid() {
        console.log("Token : ", this.authService.getTokenFromCookie())
       
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
    
        return this.httpClient.get<IUser>("http://localhost:3000/api/users/me", { headers })
    }
    getPosts(page?: number, limit?: number){

        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
    
        return this.httpClient.get<IResponse<IPosts>>(`http://localhost:3000/api/posts/list?page=${page}&limit=${limit}`, { headers });
    }

    deletePost(id: string){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.delete<IPosts>(`http://localhost:3000/api/admin/post/${id}`,{headers})
    }

    updatePost(id: string, body: IPosts){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.put<IPosts>(`http://localhost:3000/api/posts/${id}`,body,{headers})
    }
}