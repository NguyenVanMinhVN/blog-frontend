import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { IResponse } from "../interfaces/response.interface";
import { ITag } from "../interfaces/tag.interface";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn:'root'
})

export class TagstService{
    constructor(private httpClient: HttpClient, private router: Router,private authService: AuthService){

    }
    
    getTags(page: number, limit: number){
        return this.httpClient.get<IResponse<ITag>>(`http://localhost:3000/api/category/list?page=${page}&limit=${limit}`);
    }

    getTag(){
        return this.httpClient.get<IResponse<ITag>>(`http://localhost:3000/api/category/list`);
    }

    createTag(body: ITag){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.post<ITag>("http://localhost:3000/api/category", body,{ headers })
    }

    updateTag(id: string, data : ITag){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });

        return this.httpClient.put<ITag>(`http://localhost:3000/api/category/${id}`, data,{headers})
    }

    deleteAccount(id: string){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.delete<ITag>(`http://localhost:3000/api/category/${id}`,{headers})
    }

}