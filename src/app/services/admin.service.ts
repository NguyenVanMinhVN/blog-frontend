import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "./auth.service";
import { IPosts } from "../interfaces/post.interface";
import { IResponse } from "../interfaces/response.interface";
import { IUser } from "../interfaces/users.interface";

@Injectable({
    providedIn:'root'
})

export class UsersadminManagement{
    constructor(private httpClient: HttpClient, private router: Router, private authService: AuthService){

    }

    getPosts(){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
    
        return this.httpClient.get<IResponse<IPosts>>("http://localhost:3000/api/posts/list?page=1&limit=0", { headers });
    }

    getAccount(page: number, limit: number){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });

        return this.httpClient.get<IResponse<IUser>>(`http://localhost:3000/api/admin/users/list?page=${page}&limit=${limit}`, {headers});


    }

    updateAccount(id: string, data : IUser){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });

        return this.httpClient.put<IUser>(`http://localhost:3000/api/admin/users/${id}`, data,{headers})
    }

    deleteAccount(id: string){
        const headers = new HttpHeaders({
            'Authorization': `Bearer ${this.authService.getTokenFromCookie()}`
        });
        return this.httpClient.delete<IUser>(`http://localhost:3000/api/admin/users/${id}`,{headers})
    }
}