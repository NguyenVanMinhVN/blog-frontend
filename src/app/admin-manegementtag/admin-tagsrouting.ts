import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminManegementtagComponent } from './admin-manegementtag.component';
import { authGuard } from '../auth/guard/auth.guard';




const routes: Routes = [{
  path: 'tag',
  component: AdminManegementtagComponent,
  canActivate:[authGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementTagRoutingModule { }