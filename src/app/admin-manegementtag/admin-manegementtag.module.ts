import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { AdminManegementtagComponent } from './admin-manegementtag.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminManagementTagRoutingModule } from './admin-tagsrouting';
import { ModalModule } from 'ngx-bootstrap/modal';



@NgModule({
  declarations: [
    AdminManegementtagComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AdminManagementTagRoutingModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ]
})
export class AdminManegementtagModule { }
