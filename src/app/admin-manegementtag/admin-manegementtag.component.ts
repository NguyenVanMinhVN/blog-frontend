import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TagstService } from '../services/tags.service';
import { FormBuilder } from '@angular/forms';
import { ITag } from '../interfaces/tag.interface';
import { BsModalService , BsModalRef} from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-admin-manegementtag',
  templateUrl: './admin-manegementtag.component.html',
  styleUrls: ['./admin-manegementtag.component.scss']
})
export class AdminManegementtagComponent implements OnInit{
  constructor(private tagService: TagstService, private fb: FormBuilder, private modalService: BsModalService){}

  @ViewChild('udatetag')
  detailModal!: TemplateRef<any>;

  page = 1;
  limit = 10;
  totalItem = 0;

  modalRef?: BsModalRef;

  formtag = this.fb.group({
    id: null,
    name: null,
  });
  tags: ITag[]= [];
  ngOnInit(): void {
    this.getDataInit()
  }
  getDataInit(){
    this.tagService.getTags(this.page, this.limit).subscribe((res) => {
      console.log("Res : ", res);
      if (res.docs){
        console.log("res.doc ", res.docs)
        this.tags = res.docs;
        this.totalItem = res.totalDocs as any;
      } 
    });
  }
  

  pageChanged(event: PageChangedEvent): void {
    this.page = event.page;
    this.getDataInit();
  }
  onUpdate(item?: ITag){
    if (item?.id) {
      this.formtag.patchValue(item as any);
    }
    this.modalRef = this.modalService.show(this.detailModal, {
      class: 'modal-lg modal-digalog-centered',
    });
  }
  onCreate(){
    this.modalRef = this.modalService.show(this.detailModal, {
      class: 'modal-lg modal-digalog-centered',
    });
  }

  onDelete(item?:ITag){
    if (item?.id) {
      this.tagService.deleteAccount(item?.id).subscribe((result) => {
        if (result) {
          this.getDataInit()
        }
      })
    }
  }
  onSubmit(){
    const dataSend = { ...this.formtag.value as any }
    if (dataSend.id) {
      this.tagService.updateTag( dataSend.id,dataSend).subscribe((result) => {
        if (result) {
          this.getDataInit();
          this.modalRef?.hide();
        }
      })
    }
    else {
      this.tagService.createTag( dataSend).subscribe((result) => {
        if (result) {
          this.getDataInit();
          this.modalRef?.hide();
        }
      })
    }
  }
}
