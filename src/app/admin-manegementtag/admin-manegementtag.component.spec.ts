import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManegementtagComponent } from './admin-manegementtag.component';

describe('AdminManegementtagComponent', () => {
  let component: AdminManegementtagComponent;
  let fixture: ComponentFixture<AdminManegementtagComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdminManegementtagComponent]
    });
    fixture = TestBed.createComponent(AdminManegementtagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
