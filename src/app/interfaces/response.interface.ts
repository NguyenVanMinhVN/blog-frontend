export interface IResponse<T>{
    docs: T[];
    totalDocs?: number;
}