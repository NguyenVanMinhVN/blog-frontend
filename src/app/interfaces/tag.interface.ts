export interface ITag {
    id?: string;
    name?: string;
    status?: string;
    is_deleted?: boolean
}