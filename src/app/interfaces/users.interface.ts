export interface IUser{
    id?: string;
    fullname?: string;
    email?: string;
    phone?: string;
    username?: string;
    password?: string;
    is_deleted?:boolean;
    role?: `${EuserRole}`;
    status?: `${EuserStatus}`;
}
export interface IUserLogin{
    username?: string;
    password?: string;
}
export interface IUserRegister{
    username?: string,
    password?: string,
    fullname?: string,
    phone?: string,
    email?: string
}
export interface ILoginResponse{
    token?: string;
}
export enum EuserRole{
    ADMIN = "ADMIN",
    USER = "USER"
}

export enum EuserStatus {
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE"
}
