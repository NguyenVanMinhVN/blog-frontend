import { Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  form = this.fb.group({
    username: null,
    password: null,
  });

  constructor(private fb: FormBuilder, private authService: AuthService) {}
  onSubmit() {
    this.authService.login(this.form.value as any);
  }
}
