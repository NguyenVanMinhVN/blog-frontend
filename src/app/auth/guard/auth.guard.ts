import { inject } from "@angular/core"
import { ToastrService } from "ngx-toastr";
import { AuthService } from "src/app/services/auth.service"

export const authGuard=()=>{
    const auuthService = inject(AuthService);
    const toastr = inject(ToastrService);
    if(!auuthService.checkLogin()){
        toastr.warning("Caan dang nhap");
        return false;
    }
    return true;

}