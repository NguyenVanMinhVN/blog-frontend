import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  formregister = this.fb.group({
    username: null,
    password: null,
    fullname: null,
    phone: null,
    email: null,
  });

  constructor(private fb: FormBuilder, private authService: AuthService, private toastrService: ToastrService) {}
  onSubmit() {
    console.log(this.formregister.value)
    this.authService.register(this.formregister.value as any);
  }
}
