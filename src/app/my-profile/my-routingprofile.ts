import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyProfileModule } from './my-profile.module';
import { MyProfileComponent } from './my-profile.component';
import { authGuard } from '../auth/guard/auth.guard';


const routes: Routes = [{
  path: 'user',
  component: MyProfileComponent,
  canActivate:[authGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyProfileRoutingModule {}