import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { IUser } from '../interfaces/users.interface';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent {
  constructor(private myProfile: AuthService, private route: ActivatedRoute){

  }
  myInfo!: IUser;

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.myProfile.getUserId().subscribe((res) => {
      if (res){
        console.log("User : ", res)
      }
    });
  }


}
