import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostComponent } from './post.component';
import { authGuard } from '../auth/guard/auth.guard';


const routes: Routes = [{
  path: 'newpost',
  component: PostComponent,
  canActivate:[authGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostRoutingModule { }