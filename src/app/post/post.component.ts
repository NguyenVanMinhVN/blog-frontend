import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TagstService } from '../services/tags.service';
import { ITag } from '../interfaces/tag.interface';
import { PostService } from '../services/post.service';
import { IUser } from '../interfaces/users.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit{

  tags: ITag[]= [];
  user: IUser = {};

  formupost = this.fb.group({
    id: null,
    name: null,
    tag: null,
    image_url: null,
    author: this.user.id,
    tiltle: null,
    subtiltle: null,
    content: null
  });
  constructor(private fb: FormBuilder, private tagService: TagstService, private postService: PostService, private router: Router) {}
  ngOnInit(): void {
    this.tagService.getTag().subscribe((res) => {
      if (res.docs){
        this.tags = res.docs;
      } 
    });

    this.postService.get_Authorid().subscribe((res) =>{
      console.log("Res1 : ", res);
      if (res){
        console.log("res1 ", res)
        this.user = res;
      } 
    })
  }

  onCreat(){
    this.formupost.value.author = this.user.id
    console.log(this.formupost.value)
    this.postService.creatPost(this.formupost.value as any).subscribe((res) =>{
      if (res){
        console.log("Result ", res)
        this.router.navigateByUrl('/');
      } 
    })
  }

}
