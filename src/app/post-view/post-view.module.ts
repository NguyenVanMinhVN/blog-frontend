import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostViewComponent } from './post-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminManagementTagRoutingModule } from './post-viewrouting.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';



@NgModule({
  declarations: [
    PostViewComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AdminManagementTagRoutingModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ]
})
export class PostViewModule { }
