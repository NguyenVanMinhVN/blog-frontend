import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PostService } from '../services/post.service';
import { ActivatedRoute } from '@angular/router';
import { IPosts } from '../interfaces/post.interface';
import { AuthService } from '../services/auth.service';
import { Subject, takeUntil } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-post-view',
  templateUrl: './post-view.component.html',
  styleUrls: ['./post-view.component.scss']
})
export class PostViewComponent implements OnInit, OnDestroy{
  postId!: string| undefined |null;
  isAuthor = false;
  constructor( private modalService: BsModalService, private fb: FormBuilder, private postNew: PostService, private route: ActivatedRoute, private authorService: AuthService){

  }

  @ViewChild('udatetemplate')
  detailModal!: TemplateRef<any>;

  formupost = this.fb.group({
    id: null,
    name: null,
    image_url: null,
    tag: null,
    tiltle: null,
    subtiltle: null,
    content: null
  });

  newpost!: IPosts;
  destroy = new Subject();

  ngOnInit(): void {
    this.postId =  this.route.snapshot.paramMap.get('id');
    this.getData();

  }

  getData(){
    if(this.postId)
    this.postNew.getPostId(this.postId).subscribe((resPost) => {
      if (resPost){
        this.newpost = resPost;
        this.authorService.authInfo.pipe(takeUntil(this.destroy)).subscribe((resUser) => {
          console.log("newpost ", this.newpost)

          if (resUser && resUser.id === resPost.author){
            this.isAuthor = true;
          }
        });
      }
    });
  }
  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }

  modalRef?: BsModalRef;
  onUpdate(item?: IPosts){
    if (item?.id) {
      this.formupost.patchValue(item as any);
    }
    this.modalRef = this.modalService.show(this.detailModal, {
      class: 'modal-lg modal-digalog-centered',
    });
  }

  onSubmit(){
    const dataSend = { ...this.formupost.value as any }
    if (dataSend.id) {
      this.postNew.updatePost(dataSend.id, dataSend).subscribe((result) => {
        if (result) {
          // this.getDataInit();
          // this.toastrService.success(
          //   'Update news successfully',
          //   'Successfully'
          // );
          this.getData()
          this.modalRef?.hide();
        }
      })
    }
  }
}
