import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { IPosts } from 'src/app/interfaces/post.interface';
import { PostService } from 'src/app/services/post.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss'],
})
export class PostCardComponent implements OnInit {
  posts!: IPosts[];

  page = 1;
  limit = 10;
  totalItem = 0;
  modalRef?: BsModalRef;

  formupost = this.fb.group({
    id: null,
    name: null,
    tag: null,
    tiltle: null,
    subtiltle: null,
    content: null
  });
  constructor(private fb: FormBuilder, private postService: PostService, private modalService: BsModalService) {}
  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.postService.getPosts(this.page, this.limit).subscribe((res) => {
    console.log("Res : ", res);
    if (res.docs){
        console.log("res.doc ", res.docs)
        this.posts = res.docs;
        this.totalItem = res.totalDocs as any;
      } 
    });
  }

    pageChanged(event: PageChangedEvent): void {
      this.page = event.page;
      this.getData();
    }
}

