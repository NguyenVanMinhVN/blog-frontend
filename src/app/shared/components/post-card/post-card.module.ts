import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostCardComponent } from './post-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminManagementTagRoutingModule } from 'src/app/admin-manegementtag/admin-tagsrouting';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';



@NgModule({
  declarations: [
    PostCardComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AdminManagementTagRoutingModule,
    ModalModule.forRoot(),
    PaginationModule.forRoot()
  ],
  exports:[PostCardComponent]
})
export class PostCardModule { }
