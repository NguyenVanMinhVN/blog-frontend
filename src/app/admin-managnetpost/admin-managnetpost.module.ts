import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AdminManagnetpostComponent } from './admin-managnetpost.component';
import { AdminManagementPostsRoutingModule } from './admin-postsrouting.module';



@NgModule({
  declarations: [
    AdminManagnetpostComponent
  ],
  imports: [
    CommonModule,
    AdminManagementPostsRoutingModule,
    PaginationModule.forRoot()
  ]
})
export class AdminManagnetpostModule { }
