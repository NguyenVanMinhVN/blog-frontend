import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminManagnetpostComponent } from './admin-managnetpost.component';
import { authGuard } from '../auth/guard/auth.guard';




const routes: Routes = [{
  path: 'posts',
  component: AdminManagnetpostComponent,
  canActivate:[authGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementPostsRoutingModule { }
