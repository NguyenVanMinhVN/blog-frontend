import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminManagnetpostComponent } from './admin-managnetpost.component';

describe('AdminManagnetpostComponent', () => {
  let component: AdminManagnetpostComponent;
  let fixture: ComponentFixture<AdminManagnetpostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AdminManagnetpostComponent]
    });
    fixture = TestBed.createComponent(AdminManagnetpostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
