import { Component } from '@angular/core';
import { PostService } from '../services/post.service';
import { IPosts } from '../interfaces/post.interface';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

@Component({
  selector: 'app-admin-managnetpost',
  templateUrl: './admin-managnetpost.component.html',
  styleUrls: ['./admin-managnetpost.component.scss']
})
export class AdminManagnetpostComponent {
  posts!: IPosts[];
  page = 1;
  limit = 10;
  totalItem = 0;
  constructor(private postService: PostService) {}
  ngOnInit(): void {
    this.getData()
  }

  getData(){
    this.postService.getPosts(this.page, this.limit).subscribe((res) => {
      if (res.docs) this.posts = res.docs;
      this.totalItem = res.totalDocs as any;
    });
  }

  pageChanged(event: PageChangedEvent): void {
    this.page = event.page;
    this.getData();
  }
  onDelete(item?:IPosts){
    if (item?.id) {
      this.postService.deletePost(item?.id).subscribe((result) => {
        if (result) {
          this.getData()
        }
      })
    }
  }
}
