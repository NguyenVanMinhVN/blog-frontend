import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'frontend';
  id!: string|null|undefined;
  isLogined = false;
  isAdmin = false;
  destroy = new Subject();
  constructor(private authService: AuthService){}
  ngOnDestroy(): void {
    this.destroy.next(true);
    this.destroy.complete();
  }
  ngOnInit(): void {
    this.authService.getUser();
    this.authService.authInfo.pipe(takeUntil(this.destroy)).subscribe(result=>{
      if(result){
        this.isLogined = true;
        this.id = result.id;
        if(result.role === "ADMIN"){
          this.isAdmin = true;
        }
      }else{
        this.isLogined = false;
      }
    })

  }

  onClick(){
    this.authService.logOut() 
  }

}
